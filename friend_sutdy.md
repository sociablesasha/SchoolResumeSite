# 조성륜
## 10.2 ~ 10.6
## 오른쪽 마우스 금지 및 Ctrl 금지
```javascript
<script language="javascript">
        document.oncontextmenu = function() {
            return false;
        }

        function noctrl() {
            var key = event.keyCode;
            if (event.ctrlKey == true) {
                return false;
            }
        }
        document.onkeydown = noctrl;
</script>
```

## 버튼 이벤트를 통하여 원하는 태그로 이동하기
```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>wheel event</title>
    <style>
        .select {
            width: 170px;
            height: 30px;
            background-color: white;
            border: 1px solid #000;
            margin: 0 auto;
            padding: 5px;
        }
        
        #div1 {
            width: 100%;
            height: 800px;
            background-color: red;
        }
        
        #div2 {
            width: 100%;
            height: 800px;
            background-color: yellow;
        }
        
        #div3 {
            width: 100%;
            height: 800px;
            background-color: green;
        }
    </style>
</head>

<body>
    <div class="select">
        <button onclick="redfunc()">red</button>
        <button onclick="yellowfunc()">yellow</button>
        <button onclick="greenfunc()">green</button>
    </div>
    <div id="div1"></div>
    <div id="div2"></div>
    <div id="div3"></div>
    <script>
        function redfunc() {
            document.getElementById("div1").scrollIntoView();
        }

        function yellowfunc() {
            document.getElementById("div2").scrollIntoView();
        }

        function greenfunc() {
            document.getElementById("div3").scrollIntoView();
        }
    </script>
</body>
</html>
```

# 강대한
## 10.2 ~ 10.6
## 들어오는 키값 확인
```javascript
<script src="jquery-3.2.1.min.js"></script>
    <script language="JavaScript">
        <!--

        function keydonw() {
            var keycode = event.keyCode;
            var realkey = String.fromCharCode(event.keyCode);
            alert("키워드 값 : " + keycode + "\n입력키 : " + realkey);
        }

        document.onkeydown = keydonw;
        // -->
</script>
```

## 유효한 날짜 검사
```javascript
    <script>
        
        function isDateFormat(d) {
            /*입려되는 형태가 2017-5-7이와 같은 형식으로 들어오는지*/
            var df = /[0-9]{4}-[0-9]{2}-[0-9]{2}/;
            return d.match(df);
        }

        function isLeaf(year) {
            /*윤년 검사*/
            var leaf = false;

            if (year % 4 == 0) {
                leaf = true;

                if (year % 100 == 0) {
                    leaf = false;
                }

                if (year % 400 == 0) {
                    leaf = true;
                }
            }

            return leaf;
        }

        /* 날짜 검사*/
        function isValidDate(d) {
            // 포맷에 안맞으면 false리턴
            if (!isDateFormat(d)) {
                return false;
            }

            var month_day = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

            var dateToken = d.split('-');
            var year = Number(dateToken[0]);
            var month = Number(dateToken[1]);
            var day = Number(dateToken[2]);

            // 날짜가 0이면 false
            if (day == 0) {
                return false;
            }

            var isValid = false;

            // 윤년일때
            if (isLeaf(year)) {
                if (month == 2) {
                    if (day <= month_day[month - 1] + 1) {
                        isValid = true;
                    }
                } else {
                    if (day <= month_day[month - 1]) {
                        isValid = true;
                    }
                }
            } else {
                if (day <= month_day[month - 1]) {
                    isValid = true;
                }
            }

            return isValid;
        }
        alert(isValidDate('2008-12-32'));
    </script>
```

# 손상우
## 슬라이드 메뉴
```html
<!DOCTYPE html>
<html lang="en">
<head>
<title></title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/bootstrap-3.3.7-dist/css/bootstrap.css">
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="css/jquery-3.2.1.js"></script>
<script src="css/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
</head>
<style>
.wrap {
	width: 500px;
	height: auto;
	position: relative;
	display: inline-block;
}

.wrap textarea {
	width: 100%;
	resize: none;
	min-height: 4.5em;
	line-height: 1.6em;
	max-height: 9em;
}

.wrap span {
	position: absolute;
	bottom: 5px;
	right: 5px;
}

#counter {
	background: rgba(255, 0, 0, 0.5);
	border-radius: 0.5em;
	padding: 0 .5em 0 .5em;
	font-size: 0.75em;
}
</style>
<body>
	<div class="wrap">
		<textarea id="content"></textarea>
		<span id="counter">###</span>
	</div>
</body>
<script>
	$(function() {
		$('#content').keyup(function(e) {
			var content = $(this).val();
			$(this).height(((content.split('\n').length + 1) * 1.5) + 'em');
			$('#counter').html(content.length);
		});
		$('#content').keyup();
	});
</script>
</html>
```

## fullpage scroll
```html
<!DOCTYPE html>
<html>

<head>
    <title>MouseWheel</title>
    <style type="text/css">
        html,
        body {
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
        }

        .box {
            width: 100%;
            height: 100%;
            position: relative;
            color: #ffffff;
            font-size: 24pt;
        }
    </style>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script type="text/javascript">
        window.onload = function () {
            $(".box").each(function () {
                // 개별적으로 Wheel 이벤트 적용
                $(this).on("mousewheel DOMMouseScroll", function (e) {
                    e.preventDefault();
                    var delta = 0;
                    if (!event) event = window.event;
                    if (event.wheelDelta) {
                        delta = event.wheelDelta / 120;
                        if (window.opera) delta = -delta;
                    } else if (event.detail) delta = -event.detail / 3;
                    var moveTop = null;
                    // 마우스휠을 위에서 아래로
                    if (delta < 0) {
                        if ($(this).next() != undefined) {
                            moveTop = $(this).next().offset().top;
                        }
                        // 마우스휠을 아래에서 위로
                    } else {
                        if ($(this).prev() != undefined) {
                            moveTop = $(this).prev().offset().top;
                        }
                    }
                    // 화면 이동 0.8초(800)
                    $("html,body").stop().animate({
                        scrollTop: moveTop + 'px'
                    }, {
                            duration: 800, complete: function () {
                            }
                        });
                });
            });
        }
    </script>
</head>

<body>
    <div class="box" style="background-color:red;">1</div>
    <div class="box" style="background-color:orange;">2</div>
    <div class="box" style="background-color:yellow;">3</div>
    <div class="box" style="background-color:green;">4</div>
    <div class="box" style="background-color:blue;">5</div>
    <div class="box" style="background-color:indigo;">6</div>
    <div class="box" style="background-color:violet;">7</div>
</body>

</html>
```

# 성 + 대
## 10.2 ~ 10.6
## 공백, 공백 문자 제거
```html
성륜이꺼
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>nospace</title>
    
</head>

<body>
    <h2>공백 제거</h2>
    <textarea name="text" id="text" cols="30" rows="10"></textarea>
    <button onclick="nospace()">공백제거</button>
    <script>
        function nospace(){
            var text = document.getElementById('text').value;
            text = text.replace(/\s/gi,"");
            document.getElementById("text").value = text;
        }
    </script>
</body>
</html>
```
```javascript
내꺼
<script type="text/javascript">
    function blankDel(text){
        var regText = text;
    
        regText = regText.replace(/\s/gi, "");
        return window.alert(regText);
    }
    
    var a = " adf a asdf        a\nsdf s ";
    blankDel(a);
</script>
```

# 상 + 대
## 10.12
## 글자수 확인
```javascript
<script src="./jquery-3.2.1.js"></script>
<script>
	$(function() {
		$('(글자를 입력받는 태그)').keyup(function(e) {
			var content = $(this).val();
			$(this).height(((content.split('\n').length + 1) * 1.5) + 'em');
			$('(글자수 출력해주는 태그)').html(content.length);
		});
		$('(글자수 출력해주는 태그)').keyup();
	});
</script>
```